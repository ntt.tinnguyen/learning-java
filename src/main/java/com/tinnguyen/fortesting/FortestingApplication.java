package com.tinnguyen.fortesting;

import org.apache.commons.math3.util.MathUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;

@SpringBootApplication
public class FortestingApplication {

    public static void main(String[] args) {
        System.out.println("Testing");
        System.out.println(UUID.randomUUID().toString());

        boolean stop = false;
        while (!stop) {
            System.out.println("Nhap lua chon:");
            System.out.println("1. vay do");
            System.out.println("2. Exit");
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            switch (input) {
                case "1":
                    System.out.println("Choose 1");
                    break;
                case "2":
                    System.out.println("Choose 2: exit");
                    stop = true;
                    return;
                default:
                    System.out.println("Nothing");
                    continue;
            }
        }
//        int lowerBound;
//        int upperBound;
//        String currentParamsStr = "30";
//        int currentParamsIntValue = Integer.parseInt(currentParamsStr);
//        long round = Math.round(currentParamsIntValue/10.0) * 10;
//        System.out.println(String.format("rounded = %s", round));
//        int endingDigit = currentParamsIntValue % 10;
//        if (endingDigit == 5) {
//            lowerBound = currentParamsIntValue;
//            upperBound = currentParamsIntValue;
//        } else if (currentParamsIntValue < 5) {
//            lowerBound = 0;
//            upperBound = currentParamsIntValue;
//        } else if (currentParamsIntValue > 95) {
//            lowerBound = currentParamsIntValue;
//            upperBound = 100;
//        } else {
//            int roundedValue = (int) roundToNearestTen(currentParamsIntValue);
//            lowerBound = roundedValue - 4;
//            upperBound = roundedValue + 4;
//        }
//
//        List<Integer> aList = Arrays.asList(4,2,3,1);
//        System.out.println(String.format("aList=%s", aList.stream().sorted().collect(Collectors.toList())));
//        System.out.println(String.format("lowerBound=%s", lowerBound));
//        System.out.println(String.format("upperBound=%s", upperBound));
//
//        BigDecimal a = new BigDecimal("99.99");
//        BigDecimal b = new BigDecimal("134.95");
//        float d = a.floatValue() / b.floatValue();
//        System.out.println("d=" +  d);
//        System.out.println("e=" + (1 - d));
//        System.out.println(String.format("%s / %s = %s", a.floatValue(), b.floatValue(), d));
//        int c = calculateDiscountPercentage(a, b);
//        System.out.println(String.format("c=%s", c));
//        SpringApplication.run(FortestingApplication.class, args);
        
        // 1. Viet ham ktra s ng to
        // 2. Lap va tiem 10 so nguyen to
        // 3. Cong tat ca so nguyen to lai voi nhau
        
        int a = 1;
        
        int list[] = new int[10];
        
    }

    private static int calculateDiscountPercentage(BigDecimal price, BigDecimal oldPrice) {
        if (price == null || oldPrice == null
                || Objects.equals(price, BigDecimal.ZERO)
                || Objects.equals(oldPrice, BigDecimal.ZERO)
                || price.compareTo(oldPrice) >= 0) {
            return 0;
        }

        int result = (oldPrice.subtract(price)).divide(oldPrice, 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).intValue();

        BigDecimal percentage = price.divide(oldPrice, 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
        System.out.println(String.format("percentage = %s", percentage));
        System.out.println(String.format("return value = %s", result));
//        return 100 - percentage.intValue();
        return result;
    }

    private static long roundToNearestTen(double input) {
        return Math.round(input / 10.0) * 10;
    }
}
