package com.tinnguyen.fortesting.oop;

public class Dog extends Animal {
    
    private String type;
    
    public Dog() {
        super();
        super.speak();
    }
    
    // Overloading
    public void speak(boolean isOld) {
        if (isOld) {
            System.out.println("GAU GAU GAU GAU");
        } else {
            System.out.println("Gau gau");
        }
    }

        // Overloading
    public void speak() {
        // Check tuoi xem da gia hay chua
        boolean isOld;
        if (this.getAge() > 5) {
            isOld = true;
        } else {
            isOld = false;
        }

        if (isOld) {
            System.out.println("GAU GAU GAU GAU");
        } else {
            System.out.println("Gau gau");
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String toString() {
        return super.toString() + ", type=" + type;
    }
}
