package com.tinnguyen.fortesting.oop;

public class Main {

    public static void main(String[] args) {
        try {
            testing("12");
        } catch (IllegalArgumentException e) {
            System.out.println("Sai input");
        }

        Animal testingAnimal = null;

        try {
            System.out.println("Mau long cua animal testing la" + testingAnimal.getAge());
            System.out.println("ABc");
            System.out.println("ABc");
            System.out.println("ABc");
            System.out.println("123");
        } catch (NullPointerException e) {
            System.out.println("NullPointerException cmnr");
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException cmnr");
        } catch (ArithmeticException e) {
            System.out.println("ArithmeticException cmnr");
        } catch (Exception e) {
            System.out.println("General exception. Deo biet loi cmm gi nua");
            System.out.println(e);
        } finally {
            System.out.println("After try catch");
        }

        
        
        
        Animal animal1 = new Animal("red", 10);
        Animal animal2 = new Animal("brown", 5);


        animal1.ANIMAL_PROTECT_ORGANIZATION = "KEN";
        System.out.println(animal2.ANIMAL_PROTECT_ORGANIZATION);

        String[] test = "asdasd    asdasd".split(" ");
        Animal animal = new Animal();
        animal.speak();

        Cat cat = new Cat();
        cat.speak();
        System.out.println(cat.getColor());

        Dog dog = new Dog();
        dog.setAge(4);
        dog.setType("British");
        dog.speak();

        System.out.println(dog);

        Dog dog2 = new Dog();
        dog2.setAge(10);
        dog2.speak();


        Animal cat2 = new Cat();
        cat2.speak();

        // This is not a dog
        if (cat2 instanceof Dog) {
            System.out.println("This is a dog");
        } else {
            System.out.println("This is not a dog");
        }

        // This is a cat
        if (cat2 instanceof Cat) {
            System.out.println("This is a cat");
        } else {
            System.out.println("This is not a cat");
        }

        // This is an animal
        if (cat2 instanceof Animal) {
            System.out.println("This is an animal");
        } else {
            System.out.println("This is not an animal");
        }
    }

    
    private static void testing(String input) {
        // Validate input length
        if (input.length() > 3) {
            System.out.println(input);
        } else {
            throw new IllegalArgumentException("Input has to have length greater then 3.");
        }
    }
}
