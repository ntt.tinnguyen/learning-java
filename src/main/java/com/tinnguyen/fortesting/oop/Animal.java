package com.tinnguyen.fortesting.oop;

public class Animal {

    public static String ANIMAL_PROTECT_ORGANIZATION = "WSPA";


    private String color;
    private int age;
    private boolean dead;

    public Animal() {
        
    }
    
    public Animal(String color, int age) {
        this.age = age;
        this.color = color;
    }

    public void speak() {
        
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Color: " + color + ", Age: " + age;
    }
}
