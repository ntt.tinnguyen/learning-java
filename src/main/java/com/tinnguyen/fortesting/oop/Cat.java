package com.tinnguyen.fortesting.oop;

public class Cat extends Animal {

    public Cat() {
        this.setAge(1);
        this.setColor("yellow");
    }

    public void speak() {
        System.out.println("Meow meow");
    }
    
}
