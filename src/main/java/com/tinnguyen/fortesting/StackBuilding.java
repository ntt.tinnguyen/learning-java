package com.tinnguyen.fortesting;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;

@SpringBootApplication
public class StackBuilding {
    static int stack[] = new int[10];
    static int n = 0;

    public int sum(int a, int b) {
        return a + b;
    }

    public int sum(String a, String b) {
        return Integer.valueOf(a) + Integer.valueOf(b);
    }

    public static class Animal {

        private String color;

    }

    public static void main(String[] args) {
        Animal animal = new Animal();
        
        
        Integer firstPop = pop();
        System.out.println(firstPop);
        push(1);
        push(2);
        push(3);
        push(4);
        push(5);
        push(6);
        push(7);
        push(8);
        push(9);
        push(10);
        push(11);

        printStack();

        Integer pop1 = pop();
        System.out.println(pop1);
        printStack();
        push(11);
        printStack();
    }

    public static void printStack() {
        for (int i = 0; i < n; i++) {
            System.out.print(stack[i] + ", ");
        }
        System.out.println("");
    }

    public static void push(int input) {
        if (isFull()) {
            return;
        }
        stack[n] = input;
        n++;
    }

    public static Integer pop() {
        if (isEmpty()) {
            return null;
        }
        int result = stack[n - 1];
        n--;
        return result;
    }

    public static boolean isFull() {
        return n == 10;
    }

    public static boolean isEmpty() {
        return n == 0;
    }

    public static int getSize() {
        return n;
    }


}
